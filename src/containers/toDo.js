import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Words from './words';
import {Table} from 'react-bootstrap';

const ToDoList = () => {

    const [data, setData] = useState("loading..");
    const [words, setWords] = useState("");

    useEffect( () => {
        axios.get('https://jsonplaceholder.typicode.com/todos').then( resp => {
            let words = "";
            let list = resp.data.map( el => {
                 words=  words.concat(el.title, ' ');
                return <tr key={el.id}> <td>{el.id}</td> <td>{el.title} </td> </tr>
            })
            setData(list);
            setWords(words);
        })
    }, [])

    return <div className="container">
        <Table striped bordered hover>
            <thead>
                <tr>
                <th>id</th>
                <th>to do</th>
                </tr>
            </thead>
            <tbody>
                {data}
            </tbody>
        </Table>
        <div><Words data={words}/></div>
    </div>
}

export default ToDoList;