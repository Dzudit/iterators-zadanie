import React, { useEffect, useState } from 'react';
import {Table} from 'react-bootstrap';


const Words = ({data}) => {
     
    const [wordsList, setWordsList] = useState();
    const [wordsListJson, setWordsListJson] = useState();

    const countUsingMap = () => {
        let mapa = new Map();
        data.trim().split(' ').sort().forEach( word => {
            let count = 1;
            if(mapa.has(word)) {
               count = mapa.get(word) + 1;
            }
            mapa.set(word, count)
        });
        let wordsDisplay = []; 
        mapa.forEach((el, key) => {
            wordsDisplay.push(<tr key={key}> <td>{key}</td><td>{el}</td></tr>);
        })
        setWordsList(wordsDisplay);
    }

    const countUsingJson = () => {
        let result = {}
        data.trim().split(' ').sort().forEach( word => {
            if(result[word] === undefined) {
                result[word] = 1;
            }
            else {
                result[word] = result[word] + 1;
            }
        })
        setWordsListJson(result);
    }

    useEffect(()=>{
        countUsingMap();
        countUsingJson();
    }, [data])


    return <>
        <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>word</th>
                    <th>count</th>
                    </tr>
                </thead>
                <tbody>
                    {wordsList}
                </tbody>
            </Table>
        </div>
    </>
}

export default Words;