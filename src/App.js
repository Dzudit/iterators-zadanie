import React from 'react';
import './App.css';
import ToDoList from './containers/toDo';

function App() {
  return (
    <div className="App">
          <ToDoList/>
    </div>
  );
}

export default App;
